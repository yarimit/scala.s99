object P11 {
  def encodeModified[A](l: List[A]) : List[Any] = {
    val enc = P10.encode(l)
    enc.map(v => if(v._1 == 1) v._2 else v)
  }

}
