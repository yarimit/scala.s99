object P07 {
  def flatten(l: List[Any]) : List[Any] = {
    l match {
      case (x1:List[Any])::xs => flatten(x1) ++ flatten(xs)
      case x1::xs => List(x1) ++ flatten(xs)
      case Nil => Nil
    }
  }
}
