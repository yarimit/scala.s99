object P06 {
  def isPalindrome[A](l: List[A]) : Boolean = {
    def reverse[A](l: List[A]) : List[A] = {
      l match {
        case Nil => Nil
        case x::xs => reverse(xs) ++ List(x)
      }
    }

    l == reverse(l)
  }
}
