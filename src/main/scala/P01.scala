object P01 {
  def last[A](l: List[A]) : A = {
    l match {
      case x::Nil => x
      case x::xs1 => last(xs1)
      case _ => throw new IllegalArgumentException()
    }
  }
}
