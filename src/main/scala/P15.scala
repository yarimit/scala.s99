object P15 {
  def duplicateN[A](n:Int, l: List[A]) : List[A] = {
    l match {
      case Nil => Nil
      case x::xs => List.fill(n)(x) ++ duplicateN(n, xs)
    }
  }
}
