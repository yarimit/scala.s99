object P03 {
  def nth[A](n:Int, l: List[A]) : A = {
    (n, l) match {
      case (0, x::xs1) => x
      case (n, x::xs1) => nth(n - 1, xs1)
      case (_, Nil) => throw new IllegalArgumentException()
    }
  }
}
