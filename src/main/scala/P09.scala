object P09 {
  def pack[A](l: List[A]) : List[List[A]] = {
    if(l.isEmpty)
      Nil
    else {
      val (a, b) = l.span(_ == l.head)
      a :: pack(b)
    }
  }
}
