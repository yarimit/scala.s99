object P16 {
  def _drop[A](n:Int, i:Int, l:List[A]) : List[A] = {
    l match {
      case Nil => Nil
      case x::xs => if((i + 1) % n == 0) _drop(n, i+1, xs) else x :: _drop(n, i+1, xs)
    }
  }

  def drop[A](n:Int, l: List[A]) : List[A] = {
    _drop(n, 0, l)
  }
}
