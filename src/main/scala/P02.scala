object P02 {
  def penultimate[A](l:List[A]):A = {
    l match {
      case x::_::Nil => x
      case _::xs1 => penultimate(xs1)
      case _ => throw new IllegalArgumentException()
    }
  }
}
