object P17 {
  def split[A](n:Int, l:List[A]) : (List[A], List[A]) = {
    (l.take(n), l.drop(n))
  }
}
