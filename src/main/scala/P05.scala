object P05 {
  def reverse[A](l: List[A]) : List[A] = {
    l match {
      case Nil => Nil
      case x::xs => reverse(xs) ++ List(x)
    }
  }
}
