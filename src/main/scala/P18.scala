object P18 {
  def slice[A](s:Int, e:Int, l:List[A]) : List[A] = {
    l.drop(s).take(e - s)
  }
}
