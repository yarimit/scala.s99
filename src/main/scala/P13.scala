
object P13 {
  def encodeDirect[A](l: List[A]) : List[Any] = {
    if(l.isEmpty)
      Nil
    else {
      val (a, b) = l.span(_ == l.head)
      (a.length, l.head) :: encodeDirect(b)
    }
  }
}
