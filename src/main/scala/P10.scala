object P10 {
  def encode[A](l: List[A]) : List[(Int, A)] = {
    if(l.isEmpty)
      Nil
    else {
      val (a, b) = l.span(_ == l.head)
      (a.length, l.head) :: encode(b)
    }
  }
}
