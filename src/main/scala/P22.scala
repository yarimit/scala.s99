object P22 {
  def range(s:Int, e:Int) : List[Int] = {
    if(s <= e) {
      s :: range(s + 1, e)
    } else {
      Nil
    }
  }
}
