import scala.math._

object P23 {
  def randomSelect[A](n:Int, l:List[A]) : List[A] = {
    if(n == 0) {
      Nil
    } else {
      l(math.floor(math.random * l.length).toInt) :: randomSelect(n - 1, l)
    }
  }
}
