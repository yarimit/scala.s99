object P20 {
  def removeAt[A](n:Int, l:List[A]) : (List[A], A) = {
    val l2 = l.take(n) ++ l.takeRight(l.length - (n + 1))
    (l2, l(n))
  }
}
