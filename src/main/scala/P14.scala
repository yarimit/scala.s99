object P14 {
  def duplicate[A](l: List[A]) : List[A] = {
    l match {
      case Nil => Nil
      case x::xs => List(x, x) ++ duplicate(xs)
    }
  }
}
