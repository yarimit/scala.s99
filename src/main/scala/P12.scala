object P12 {
  def decode[A](l: List[(Int, A)]) : List[Any] = {
    l match {
      case Nil => Nil
      case (x : (Int, A))::xs =>
        List.fill(x._1)(x._2) ++ decode(xs)
    }
  }
}
