object P19 {
  def rotate[A](n:Int, l:List[A]) : List[A] = {
    if(n > 0) {
      l.drop(n) ++ l.take(n)
    } else {
      l.takeRight(-n) ++ l.dropRight(-n)
    }
  }
}
