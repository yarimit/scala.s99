object P21 {
  def insertAt[A](c:A, n:Int, l:List[A]) : List[A] = {
    l.take(n) ++ List(c) ++ l.takeRight(l.length - n)
  }
}
