object P08 {
  def compress[A](l: List[A]) : List[A] = {
    l match {
      case Nil => Nil
      case x::xs => if(!xs.isEmpty && x == xs.head) {
        compress(xs)
      } else {
        List(x) ++ compress(xs)
      }
    }
  }
}
