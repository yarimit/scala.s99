object P04 {
  def length[A](l: List[A]) : Int = {
    l match {
      case Nil => 0
      case x::xs1 => 1 + length(xs1)
    }
  }
}
