import org.specs2.mutable._

class P16Spec extends Specification {
  "P16" should {
    "P16" in {
      P16.drop(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) must_==
        List('a, 'b, 'd, 'e, 'g, 'h, 'j, 'k)
    }
  }
}
