import org.specs2.mutable._

class P20Spec extends Specification {
  "P20" should {
    "P20" in {
      P20.removeAt(1, List('a, 'b, 'c, 'd)) must_==
        (List('a, 'c, 'd),'b)
    }

  }
}
