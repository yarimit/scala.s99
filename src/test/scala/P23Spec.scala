import org.specs2.mutable._

class P23Spec extends Specification {
  "P23" should {
    "P23" in {
      val l = List('a, 'b, 'c, 'd, 'f, 'g, 'h)
      val r = P23.randomSelect(3, l)

      for (elem <- r) {
        l.contains(elem) must_== true
      }
      true
    }
  }
}
