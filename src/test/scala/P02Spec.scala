import org.specs2.mutable._

class P02Spec extends Specification {
  "P02" should {

    "P02" in {
      P02.penultimate(List(1, 1, 2, 3, 5, 8)) must_== 5
    }

    "1要素" in {
      P02.penultimate(List(1)) must throwA[IllegalArgumentException]
    }

  }

}
