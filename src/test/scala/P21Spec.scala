import org.specs2.mutable._

class P21Spec extends Specification {
  "P21" should {
    "P21" in {
      P21.insertAt('new, 1, List('a, 'b, 'c, 'd)) must_==
        List('a, 'new, 'b, 'c, 'd)
    }
  }
}
