import org.specs2.mutable._

class P03Spec extends Specification {
  "P03" should {

    "P03" in {
      P03.nth(2, List(1, 1, 2, 3, 5, 8)) must_== 2
    }

    "範囲外" in {
      P03.nth(6, List(1, 1, 2, 3, 5, 8)) must throwA[IllegalArgumentException]
    }

  }

}
