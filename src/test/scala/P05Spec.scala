import org.specs2.mutable._

class P05Spec extends Specification {
  "P05" should {

    "P05" in {
      P05.reverse(List(1, 1, 2, 3, 5, 8)) must_== List(8, 5, 3, 2, 1, 1)
    }

  }

}
