import org.specs2.mutable._

class P18Spec extends Specification {
  "P18" should {
    "P18" in {
      P18.slice(3, 7, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) must_==
        List('d, 'e, 'f, 'g)
    }
  }
}
