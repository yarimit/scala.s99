import org.specs2.mutable._

class P14Spec extends Specification {
  "P14" should {
    "P14" in {
      P14.duplicate(List('a, 'b, 'c, 'c, 'd)) must_==
        List('a, 'a, 'b, 'b, 'c, 'c, 'c, 'c, 'd, 'd)
    }
  }
}
