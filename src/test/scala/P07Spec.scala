import org.specs2.mutable._

class P07Spec extends Specification {
  "P07" should {

    "P07" in {
      P07.flatten(List(List(1, 1), 2, List(3, List(5, 8)))) must_== List(1, 1, 2, 3, 5, 8)
    }
  }
}
