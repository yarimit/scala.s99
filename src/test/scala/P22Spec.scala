import org.specs2.mutable._

class P22Spec extends Specification {
  "P22" should {
    "P22" in {
      P22.range(4, 9) must_==
        List(4, 5, 6, 7, 8, 9)
    }
  }
}
