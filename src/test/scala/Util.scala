object Util {
  def printList[A](args: List[A]): Unit = {
    args.foreach(println)
  }
}
