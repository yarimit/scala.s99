import org.specs2.mutable._

class P15Spec extends Specification {
  "P15" should {
    "P15" in {
      P15.duplicateN(3, List('a, 'b, 'c, 'c, 'd)) must_==
       List('a, 'a, 'a, 'b, 'b, 'b, 'c, 'c, 'c, 'c, 'c, 'c, 'd, 'd, 'd)
    }
  }
}
