import org.specs2.mutable._

class P06Spec extends Specification {
  "P06" should {

    "P06 odd" in {
      P06.isPalindrome(List(1, 2, 3, 2, 1)) must_== true
    }

    "P06 even" in {
      P06.isPalindrome(List(1, 2, 3, 3, 2, 1)) must_== true
    }

    "P06 odd false" in {
      P06.isPalindrome(List(1, 2, 3, 2, 2)) must_== false
    }

    "P06 even false" in {
      P06.isPalindrome(List(1, 2, 3, 3, 2, 2)) must_== false
    }
  }
}
