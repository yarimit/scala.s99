import org.specs2.mutable._

class P04Spec extends Specification {
  "P04" should {

    "P04" in {
      P04.length(List(1, 1, 2, 3, 5, 8)) must_== 6
    }

  }

}
