import org.specs2.mutable._

class P08Spec extends Specification {
  "P08" should {

    "P08" in {
      P08.compress(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) must_== List('a, 'b, 'c, 'a, 'd, 'e)
    }
  }
}
