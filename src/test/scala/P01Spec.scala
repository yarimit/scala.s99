import org.specs2.mutable._

class P01Spec extends Specification {
  "P01" should {

    "P01" in {
      P01.last(List(1,1,2,3,5,8)) must_== 8
    }

    "1要素" in {
      P01.last(List(1)) must_== 1
    }

    "0要素" in {
      P01.last(List[Int]()) must throwA[IllegalArgumentException]
    }
  }
}
